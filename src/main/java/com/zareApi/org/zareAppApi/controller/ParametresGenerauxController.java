package com.zareApi.org.zareAppApi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.zareApi.org.zareAppApi.entities.Parametres;
import com.zareApi.org.zareAppApi.repositories.ParametresRepository;
import com.zareApi.org.zareAppApi.services.ParametresService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ParametresGenerauxController {
	
	@Autowired
	ParametresRepository parametresRepository;
	
	@Autowired
	ParametresService parametresService;
	
	@GetMapping("/parametres")
	public ResponseEntity<Parametres> getParametresGeneraux() {
//		Parametres parametresGeneraux = new Parametres();
		try {
			Parametres parametresGeneraux = parametresRepository.getOne(1);
			
			if(parametresGeneraux.getIdParamG() == 0) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<Parametres>(parametresGeneraux, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping("/parametres-ajout")
	@ResponseStatus(HttpStatus.CREATED)
	public void ajouterParametresGeneraux(@RequestBody Parametres parametres) {
			parametres.setIdParamG(1);
			parametresRepository.saveAndFlush(parametres);
	}
	
	@GetMapping("/parametres-jours-ouverts")
	public ResponseEntity<boolean[]> getJoursOuverts(){
		try {
			boolean[] joursOuverts = new boolean[7];
			joursOuverts = parametresService.getOpeningDays();
			
			return new ResponseEntity<boolean[]>(joursOuverts, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
