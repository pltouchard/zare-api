package com.zareApi.org.zareAppApi.controller;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zareApi.org.zareAppApi.entities.Creneau;
import com.zareApi.org.zareAppApi.repositories.CreneauRepository;
import com.zareApi.org.zareAppApi.repositories.ParametresParticuliersRepository;
import com.zareApi.org.zareAppApi.repositories.ParametresRepository;
import com.zareApi.org.zareAppApi.services.impl.CreneauxServiceImpl;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CreneauController {

	@Autowired
	CreneauRepository creneauRepository;
	
	@Autowired
	ParametresParticuliersRepository parametresParticuliersRepository;
	
	@Autowired
	ParametresRepository parametresRepository;
	
	@Autowired
	CreneauxServiceImpl creneauxServiceImpl;
	
	/**
	 * 
	 * @param heureDebut
	 * @param heureFin
	 * @return Tous les créneaux si les heures de début et de fin ne sont pas renséignées et un créneau si l'heure de début et de fin sont renseignés
	 */
	@GetMapping("/creneaux")
	public ResponseEntity<List<Creneau>> getAllCreneaux(@RequestParam(required = false) Time heureDebut, Time heureFin) {
		try {

			List<Creneau> creneaux = new ArrayList<Creneau>();
			Creneau creneauClaimed = null;
			
			if(heureDebut == null || heureFin == null) {
				creneauRepository.findAll().forEach(creneaux::add);
			}else {
				creneauClaimed = creneauRepository.creneauClaimed(heureDebut, heureFin);
				creneaux.add(creneauClaimed);				
			}
			
			if(creneaux.isEmpty() && creneauClaimed == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<>(creneaux, HttpStatus.OK);
		} catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * 
	 * @param date
	 * @return Liste des Créneaux conditionnés par l'ouverture de l'établissement le midi ou le soir
	 */
	@GetMapping("/creneaux-date")
	public ResponseEntity<List<Creneau>> getAllCreneauxByDate(@RequestParam(required=true) String date){
		try {
			Date dateToCheck = Date.valueOf(date);
			List<Creneau> creneaux = new ArrayList<Creneau>();
			creneaux = creneauxServiceImpl.getListeCreneauxByDateAndDispo(dateToCheck);
			if(creneaux.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<>(creneaux, HttpStatus.OK);
		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return Un créneau dont l'id est le paramètre id
	 */
	@GetMapping("/creneau/{id}")
	public ResponseEntity<Creneau> getCreneauById(@PathVariable int id){
		
		try {
			Creneau creneauARetourner = null;
			 
			if(id > 0) {
				creneauARetourner = creneauRepository.findById(id);
			}
			
			if(creneauARetourner == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<>(creneauARetourner, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	/**
	 * 
	 * @param creneau
	 * @return Si un creneau existe déjà il est retourné sinon il est créé
	 */
	@PostMapping("/creneau-ajout")
	public ResponseEntity<Creneau> ajouterCreneau(@RequestBody Creneau creneau) {
		//check if creneau exists
		Creneau checkedCreneau = creneauRepository.creneauClaimed(creneau.getCreneauDebut(), creneau.getCreneauFin());
		
		if(checkedCreneau != null) {
			return new ResponseEntity<>(checkedCreneau, HttpStatus.OK);
		}else if(creneau.getIdCreneau() == 0 && checkedCreneau == null) {
			creneauRepository.save(creneau);
			return new ResponseEntity<>(creneau, HttpStatus.CREATED);
		}else {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return ok si le créneau a bien été supprimé 
	 */
	@DeleteMapping(value = "/creneau-suppression/{id}")
	public ResponseEntity<Creneau> deleteCreneau(@PathVariable int id){
		try {
			creneauRepository.deleteById(id);
		}catch (IllegalArgumentException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
