package com.zareApi.org.zareAppApi.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

public class RapportsController {
	
	@Autowired
	private DataSource dataSource;
	
	private JasperReport rapportExcel;
	
	@PostConstruct
	public void compilerRapportExcel() throws Exception {
		InputStream modeleInputStream = this.getClass().getResourceAsStream("/populations.jrxml");
		rapportExcel = JasperCompileManager.compileReport(modeleInputStream);
	}
	
	@PostMapping(path="/generation-rapport-jour.xlsx")
	public void produireRapportExcel(@RequestParam(required = true) String dateSelected, OutputStream out, HttpServletResponse response) throws Exception {
	
		try(Connection connection = dataSource.getConnection()) {
		  Map<String, Object> parameters = new HashMap<>();
		  JasperPrint print = JasperFillManager.fillReport(rapportExcel, parameters, connection);
		
		  
		
		  JRXlsxExporter xlsxExporter = new JRXlsxExporter();
		  xlsxExporter.setExporterInput(new SimpleExporterInput(print));
		  xlsxExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
		  xlsxExporter.exportReport();
		//response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
