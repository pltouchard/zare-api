package com.zareApi.org.zareAppApi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.zareApi.org.zareAppApi.entities.ApplicationInfos;
import com.zareApi.org.zareAppApi.entities.Reservation;
import com.zareApi.org.zareAppApi.repositories.ApplicationInfosRepository;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ApplicationInfosController {
	
	@Autowired
	ApplicationInfosRepository applicationInfosRepository;
	
	/**
	 * 
	 * @param id
	 * @return application_infos bdd
	 */
	@GetMapping("/application-infos/{id}")
	public ResponseEntity<ApplicationInfos> getApplicationInfosById(@PathVariable int id) {
		try {
			ApplicationInfos applicationInfosARetourner = null;
		
			if(id > 0) {
				applicationInfosARetourner = applicationInfosRepository.findById(id);
			}
			
			if(applicationInfosARetourner == null) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<>(applicationInfosARetourner, HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Ajouter ou modifier les infos de l'application
	 * @param ApplicationInfos
	 */
	@PostMapping("/application-infos-update")
	@ResponseStatus(HttpStatus.CREATED)
	public void ajouterReservation(@RequestBody ApplicationInfos applicationInfos) {
		if(applicationInfos.getId() == 0) {
			applicationInfosRepository.save(applicationInfos);
		}else {
			applicationInfosRepository.update(applicationInfos.isParamGenerauxSet(), applicationInfos.getSetParamGeneraux(), applicationInfos.getId());
		}
	}
}
