package com.zareApi.org.zareAppApi.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zareApi.org.zareAppApi.entities.Creneau;
import com.zareApi.org.zareAppApi.entities.ParametresParticuliers;
import com.zareApi.org.zareAppApi.entities.ParametresParticuliersDto;
import com.zareApi.org.zareAppApi.repositories.CreneauRepository;
import com.zareApi.org.zareAppApi.repositories.ParametresParticuliersRepository;
import com.zareApi.org.zareAppApi.services.impl.ParametresParticuliersServiceImpl;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ParametresParticulierController {
	
	@Autowired
	ParametresParticuliersRepository parametresParticuliersRepository;
	
	@Autowired
	ParametresParticuliersServiceImpl parametresParticuliersServiceImpl;
	
	@Autowired
	CreneauRepository creneauRepository;
	
	@GetMapping("/all-parametres-particuliers")
	public ResponseEntity<List<ParametresParticuliers>> getAllParametresParticuliers(){
		try {
			List<ParametresParticuliers> listAllParametresParticuliers = parametresParticuliersRepository.findAll();
			
			if(listAllParametresParticuliers.isEmpty()) {
				return new ResponseEntity<List<ParametresParticuliers>>(listAllParametresParticuliers, HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<List<ParametresParticuliers>>(listAllParametresParticuliers, HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/parametres-particuliers")
	public ResponseEntity<List<ParametresParticuliers>> getParametresParticuliers(@RequestParam(required = true) String dateParamPDate1, @RequestParam(required = false) String dateParamPDate2) {
		try {
			List<ParametresParticuliers> listParametresParticuliers = new ArrayList<ParametresParticuliers>();
			Date startDateToGet = Date.valueOf(dateParamPDate1);
			
			if(dateParamPDate2 != null && !dateParamPDate2.trim().isEmpty()) {
				Date endDateToGet = Date.valueOf(dateParamPDate2);
				parametresParticuliersRepository.findAllParametresParticuliersByDate(startDateToGet, endDateToGet).forEach(listParametresParticuliers::add);
			}else {
				parametresParticuliersRepository.findParametresParticuliersByDate(startDateToGet).forEach(listParametresParticuliers::add);
			}
	
			if(listParametresParticuliers.isEmpty()) {
				return new ResponseEntity<List<ParametresParticuliers>>(listParametresParticuliers, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<ParametresParticuliers>>(listParametresParticuliers, HttpStatus.OK);
		
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@PostMapping("/parametres-particuliers-plage")
	public ResponseEntity<ParametresParticuliersDto> ajouterPlageParametresParticuliers(@RequestBody ParametresParticuliersDto parametresParticuliersDto){
//		ParametresParticuliersDto parametresParticuliersDto = new ParametresParticuliersDto();
//		ArrayList<String> testDates = new ArrayList<String>();
//		testDates.add("2020-06-10");
//		testDates.add("2020-06-15");
//		Creneau creneau1 = creneauRepository.findById(64);
//		Creneau creneau2 = creneauRepository.findById(61);
//		ParametresParticuliers testParams = new ParametresParticuliers(new Date(2020-06-11), 1, 0, creneau1, 10, null, 0, null, 0, creneau2, 10, null, 0, null, 0);
//		parametresParticuliersDto.setDates(testDates);
//		parametresParticuliersDto.setParametreParticulier(testParams);
		
		try {
			boolean ajoutParamsParticuliers = parametresParticuliersServiceImpl.ajouterPlageDeDates(parametresParticuliersDto);
			if(ajoutParamsParticuliers) {
				return new ResponseEntity<ParametresParticuliersDto>(parametresParticuliersDto, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/parametres-particuliers-multi")
	public ResponseEntity<ParametresParticuliersDto> ajouterParametresParticuliers(@RequestBody ParametresParticuliersDto parametresParticuliersDto) {
		try {
			boolean ajoutParamsParticuliers = parametresParticuliersServiceImpl.ajouterMultiDates(parametresParticuliersDto);
			if(ajoutParamsParticuliers) {
				return new ResponseEntity<ParametresParticuliersDto>(parametresParticuliersDto, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
