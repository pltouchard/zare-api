package com.zareApi.org.zareAppApi.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.mail.internet.AddressException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zareApi.org.zareAppApi.entities.Reservation;
import com.zareApi.org.zareAppApi.repositories.CreneauRepository;
import com.zareApi.org.zareAppApi.repositories.ReservationRepository;
import com.zareApi.org.zareAppApi.services.SendEmail;
import com.zareApi.org.zareAppApi.services.impl.ReservationServiceImpl;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ReservationController {
	
	@Autowired
	ReservationRepository reservationRepository;
	
	@Autowired
	CreneauRepository creneauRepository;
	
	@Autowired
	ReservationServiceImpl reservationServiceImpl;
	
	@Value("${owner.email}")
	private String ownerEmail;
	
	/**
	 * Récupérer toutes les réservations par nom
	 * @param nomResa
	 * @return All reservations by nom if nomResa is empty or reservation with nomResa = parameter if nomResa not empty
	 */
	@GetMapping("/reservations-nom")
	public ResponseEntity<List<Reservation>> getAllReservationsByName(@RequestParam(required = false) String nomResa) {
		try {
			List<Reservation> reservations = new ArrayList<Reservation>();
			
			if(nomResa == null) {
				reservationRepository.findAll().forEach(reservations::add);
			}else {
				reservationRepository.findByNomResaContaining(nomResa).forEach(reservations::add);
			}
			
			if(reservations.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<>(reservations, HttpStatus.OK);
		} catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}
	
	
	/**
	 * Récupérer les réservations par id
	 * @param idResa
	 * @return Réservation which id is param in url ../reservations-id?idResa=param
	 */
	@GetMapping("/reservations-id")
	public ResponseEntity<Optional<Reservation>> getAllReservationsById(@RequestParam(required = true) int idResa) {
		try {
			Optional<Reservation> reservation;
			
			if(idResa > 0) {
				reservation = reservationRepository.findById(idResa);
			}else {
				reservation = null;
			}
			
			if(reservation.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			
			return new ResponseEntity<>(reservation, HttpStatus.OK);
		} catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}
	
	/**
	 * Récupérer toutes les réservations par nom
	 * @param dateResa
	 * @return All reservations by date if dateResa is empty or reservations with dateResa = parameter if dateREsa not empty
	 */
	@GetMapping("/reservations-date")
	public ResponseEntity<List<Reservation>> getAllReservationsByDate(@RequestParam(required = false) String dateResa) {
		
		try {
	
			List<Reservation> reservations = new ArrayList<Reservation>();
			
			if(dateResa == null) {
				reservationRepository.findAll().forEach(reservations::add);
			}else {
				Date dateConverted = Date.valueOf(dateResa);
				reservationRepository.findByDateResaContaining(dateConverted).forEach(reservations::add);
			}
			
			if(reservations.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}	
			
			return new ResponseEntity<>(reservations, HttpStatus.OK);
		} catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}
	
	/**
	 * Récupérer toutes les réservations sur une plage de dates
	 * @param dateDebut + dateFin
	 * @return All reservations in a date range
	 */
	@GetMapping("/reservations-plage-date")
	public ResponseEntity<List<Reservation>> getAllReservationsByPlageDates(@RequestParam(required = true) String dateDebut, @RequestParam(required = true) String dateFin) {
		
		try {
	
			List<Reservation> reservations = new ArrayList<Reservation>();
			
			if(dateDebut != null && dateFin != null) {
				Date dateDebutConverted = Date.valueOf(dateDebut);
				Date dateFinConverted = Date.valueOf(dateFin);
				reservations = reservationRepository.findReservationByPlageDates(dateDebutConverted, dateFinConverted);
			}
			
			if(reservations.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}	
			
			return new ResponseEntity<>(reservations, HttpStatus.OK);
		} catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}
	
	/**
	 * Ajouter une réservation ou modifier une réservation
	 * @param reservation
	 */
	@PostMapping("/reservation-ajout")
//	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<String> ajouterReservation(@RequestBody Reservation reservation) {
		if(reservation.getIdResa() == 0) {
			try {
				if(reservationServiceImpl.ajoutReservation(reservation, 0)) {
					reservationRepository.save(reservation);
					
					//send email confirmation
					SendEmail sendEmail = new SendEmail(reservation.getMailResa(), "mailto:" + ownerEmail + "?subject=Annulation%20réservation&body=Annulation%20de%20la%20réservation%20%3D%20Nom%20:" + reservation.getNomResa() + ",%20Couverts%20:%20" + reservation.getNbCouvertsResa() + ",%20téléphone%20:%20" + reservation.getTelephoneResa());
					try {
						sendEmail.sendEmail();
					} catch (AddressException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					return new ResponseEntity<String>(reservation.toString(), HttpStatus.CREATED);
				}else {
					return new ResponseEntity<String>("Fermé / Complet pour la date et/ou le créneau sélectionné", HttpStatus.BAD_REQUEST);
				}
			} catch (IOException e) {
				e.printStackTrace();
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		}else {
			try {
				if(reservationServiceImpl.ajoutReservation(reservation, reservation.getIdResa())) {
					reservationRepository.update(reservation.getDateResa(), reservation.getNomResa(), reservation.getTelephoneResa(), reservation.getMailResa(), reservation.getNbCouvertsResa(), reservation.getCreneauResa().getIdCreneau(), reservation.getServiceResa(), reservation.isShowResa(), reservation.getNotesResa(), reservation.isNotesResaImportant(), reservation.isHabitue(), reservation.getIdResa());
					return new ResponseEntity<String>(reservation.toString(), HttpStatus.CREATED);
				}else {
					return new ResponseEntity<String>("Fermé / Complet pour la date et le créneau sélectionné", HttpStatus.BAD_REQUEST);
				}
			} catch (IOException e) {
				e.printStackTrace();
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	
	/**
	 * Supprimer une réservation
	 * @param id
	 * @return réponse Http
	 */
	@DeleteMapping(value = "/reservation-suppression/{id}")
    public ResponseEntity<Reservation> deleteReservation(@PathVariable int id) {

		try {
			reservationRepository.deleteById(id);
		}catch (IllegalArgumentException e) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
    }
	
}
