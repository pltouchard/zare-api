package com.zareApi.org.zareAppApi.services;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Value;

public class SendEmail {

	private String dest;
	private String infoSuppr;
	@Value("${owner.email}")
	private String ownerEmail;
	
	
	public SendEmail(String dest, String linkSuppr) {
		super();
		this.dest = dest;
		this.infoSuppr = linkSuppr;
	}



	public void sendEmail() throws AddressException {
		 // Add recipient
		 String to = this.dest;
		 InternetAddress[] myCcList = InternetAddress.parse("touchardpl@gmail.com");
		 
		// Add sender
		 String from = "zare@gmail.com";
		 final String username = "lefoudupoitou@gmail.com";
		 final String password = "^^Gmail2020!";

		String host = "smtp.gmail.com";

		Properties props = new Properties();
		 props.put("mail.smtp.auth", "true");
		 props.put("mail.smtp.starttls.enable", "true"); 
		 props.put("mail.smtp.host", host);
		 props.put("mail.smtp.port", "587");

		// Get the Session object
		 Session session = Session.getInstance(props,
		 new javax.mail.Authenticator() {
		 protected PasswordAuthentication getPasswordAuthentication() {
		 return new PasswordAuthentication(username, password);
		 }
		 });

		try {
		 // Create a default MimeMessage object
		 Message message = new MimeMessage(session);
		 
		 message.setFrom(new InternetAddress(from));
		 
		 message.setRecipients(Message.RecipientType.TO,
		 InternetAddress.parse(to));
		 message.addRecipients(Message.RecipientType.CC, myCcList);
		 // Set Subject
		 message.setSubject("Confirmation de réservation");
		 
		 message.setContent(
	              "<h1>Merci pour votre réservation !</h1>"
	              + "<p>En cas de problème vous pouvez toujours nous contacter directement au 05 49 88 42 10 </p>"
	              + "<br/>"
	              + "<p>Si vous pensez ne pas pouvoir honnorer votre réservation, pensez à nous, annulez-la ! Rien de plus simple, cliquez sur le lien ci-dessous pour envoyer un mail : </p>"
	              + "<a href='" + this.infoSuppr + "'>Supprimer la réservation</a>",
	             "text/html");

		// Send message
		 Transport.send(message);

		System.out.println("Sent message successfully....");

		} catch (MessagingException e) {
		 throw new RuntimeException(e);
		 }
		}



	public String getDest() {
		return dest;
	}



	public void setDest(String dest) {
		this.dest = dest;
	}



	public String getLinkSuppr() {
		return infoSuppr;
	}



	public void setLinkSuppr(String linkSuppr) {
		this.infoSuppr = linkSuppr;
	}

	
}
