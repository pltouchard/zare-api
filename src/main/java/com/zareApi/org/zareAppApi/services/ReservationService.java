package com.zareApi.org.zareAppApi.services;

import java.io.IOException;

import com.zareApi.org.zareAppApi.entities.Reservation;

public interface ReservationService {

	public boolean ajoutReservation(Reservation validateReservation, int reservationIdPending) throws IOException, InterruptedException;
	
}
