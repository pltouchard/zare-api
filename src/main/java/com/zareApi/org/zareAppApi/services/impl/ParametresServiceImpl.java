package com.zareApi.org.zareAppApi.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zareApi.org.zareAppApi.entities.Parametres;
import com.zareApi.org.zareAppApi.repositories.ParametresRepository;
import com.zareApi.org.zareAppApi.services.ParametresService;

@Service
public class ParametresServiceImpl implements ParametresService {
	
	@Autowired
	ParametresRepository parametresRepository;
	
	@Override
	public boolean[] getOpeningDays() {
		boolean[] joursOuverts = new boolean[7];
		Parametres parametresGeneraux = parametresRepository.getOne(1);
		
		if(!parametresGeneraux.isOuvertDimancheMidiG() && !parametresGeneraux.isOuvertDimancheSoirG()) {
			joursOuverts[0] = false; 
		}else {
			joursOuverts[0] = true;
		}
		if(!parametresGeneraux.isOuvertLundiMidiG() && !parametresGeneraux.isOuvertLundiSoirG()) {
			joursOuverts[1] = false; 
		}else {
			joursOuverts[1] = true;
		}
		if(!parametresGeneraux.isOuvertMardiMidiG() && !parametresGeneraux.isOuvertMardiSoirG()) {
			joursOuverts[2] = false; 
		}
		else {
			joursOuverts[2] = true;
		}
		if(!parametresGeneraux.isOuvertMercrediMidiG() && !parametresGeneraux.isOuvertMercrediSoirG()) {
			joursOuverts[3] = false; 
		}
		else {
			joursOuverts[3] = true;
		}
		if(!parametresGeneraux.isOuvertJeudiMidiG() && !parametresGeneraux.isOuvertJeudiSoirG()) {
			joursOuverts[4] = false; 
		}else {
			joursOuverts[4] = true;
		}
		if(!parametresGeneraux.isOuvertVendrediMidiG() && !parametresGeneraux.isOuvertVendrediSoirG()) {
			joursOuverts[5] = false; 
		}else {
			joursOuverts[5] = true;
		}
		if(!parametresGeneraux.isOuvertSamediMidiG() && !parametresGeneraux.isOuvertSamediSoirG()) {
			joursOuverts[6] = false; 
		}
		else {
			joursOuverts[6] = true;
		}
		
		
		
		return joursOuverts;
	}
}
