package com.zareApi.org.zareAppApi.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zareApi.org.zareAppApi.entities.Parametres;
import com.zareApi.org.zareAppApi.entities.ParametresParticuliers;
import com.zareApi.org.zareAppApi.entities.Reservation;
import com.zareApi.org.zareAppApi.repositories.ParametresParticuliersRepository;
import com.zareApi.org.zareAppApi.repositories.ParametresRepository;
import com.zareApi.org.zareAppApi.repositories.ReservationRepository;
import com.zareApi.org.zareAppApi.services.ReservationService;

@Service
public class ReservationServiceImpl implements ReservationService {

	@Autowired
	ReservationRepository reservationRepository;
	
	@Autowired
	ParametresRepository parametresRepository;
	
	@Autowired
	ParametresParticuliersRepository parametresParticuliersRepository;
	
	@Override
	public boolean ajoutReservation(Reservation validateReservation, int reservationIdPending) throws IOException, InterruptedException {
		boolean response = false;
		Parametres parametresApp = new Parametres();
		//get all reservations of the selected day
		List<Reservation> reservationsDay = reservationRepository.findByDateResaContaining(validateReservation.getDateResa());
		
		//get all parametres particuliers if reservation date = idParamP
		List<ParametresParticuliers> parametresParticuliers = parametresParticuliersRepository.findParametresParticuliersByDate(validateReservation.getDateResa());
		
		//if there's paramètres particulier for this day then get them
		if(parametresParticuliers.size() > 0) {
			
			ParametresParticuliers ppFind = parametresParticuliers.get(0);
			
			parametresApp.setCreneau1Midi(ppFind.getCreneau1Midi());
			parametresApp.setCreneau1MidiNbResas(ppFind.getCreneau1MidiNbResas());
			parametresApp.setCreneau2Midi(ppFind.getCreneau2Midi());
			parametresApp.setCreneau2MidiNbResas(ppFind.getCreneau2MidiNbResas());
			parametresApp.setCreneau3Midi(ppFind.getCreneau3Midi());
			parametresApp.setCreneau3MidiNbResas(ppFind.getCreneau3MidiNbResas());
			parametresApp.setCreneau1Soir(ppFind.getCreneau1Soir());
			parametresApp.setCreneau1SoirNbResas(ppFind.getCreneau1SoirNbResas());
			parametresApp.setCreneau2Soir(ppFind.getCreneau2Soir());
			parametresApp.setCreneau2SoirNbResas(ppFind.getCreneau2SoirNbResas());
			parametresApp.setCreneau3Soir(ppFind.getCreneau3Soir());
			parametresApp.setCreneau3SoirNbResas(ppFind.getCreneau3SoirNbResas());
			
			//check and return the answer if the day selected it's when it's closed
			if(!parametresParticuliers.get(0).isOuvertMidiP() && validateReservation.getServiceResa() == "midi") {
				return response = false;
			}
			if(!parametresParticuliers.get(0).isOuvertSoirP() && validateReservation.getServiceResa() == "soir") {
				return response = false;
			}
		}else {
		
			//get all parametres généraux
			parametresApp = parametresRepository.getOne(1);
			
			
			//check opening days by service
			Boolean[] openingListMidi = new Boolean[] {
					parametresApp.isOuvertDimancheMidiG(),
					parametresApp.isOuvertLundiMidiG(), 
					parametresApp.isOuvertMardiMidiG(),
					parametresApp.isOuvertMercrediMidiG(),
					parametresApp.isOuvertJeudiMidiG(),
					parametresApp.isOuvertVendrediMidiG(),
					parametresApp.isOuvertSamediMidiG()
			};
			
			Boolean[] openingListSoir = new Boolean[] {
					parametresApp.isOuvertDimancheSoirG(),
					parametresApp.isOuvertLundiSoirG(),
					parametresApp.isOuvertMardiSoirG(),
					parametresApp.isOuvertMercrediSoirG(),
					parametresApp.isOuvertJeudiSoirG(),
					parametresApp.isOuvertVendrediSoirG(),
					parametresApp.isOuvertSamediSoirG()
					
			};
			
	
			//check if it's an opening day of the week
			final Calendar calendar = new GregorianCalendar();
			calendar.setTime(validateReservation.getDateResa());
			int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
			if(validateReservation.getServiceResa().equals("midi")) {
				if(openingListMidi[day] != true) {
					return response = false;
				}
			}
			if(validateReservation.getServiceResa().equals("soir")) {
				if(openingListSoir[day] != true) {
					return response = false;
				}
			}
	
		}
		
		//declare all counters
		int nmbreCouvertsJour = 0;
		int nmbreCouvertsCreneau1Midi = 0;
		int nmbreCouvertsCreneau2Midi = 0;
		int nmbreCouvertsCreneau3Midi = 0;
		int nmbreCouvertsCreneau1Soir = 0;
		int nmbreCouvertsCreneau2Soir = 0;
		int nmbreCouvertsCreneau3Soir = 0;
		
		//increment all counters
		if(reservationsDay.size() > 0) {
			for(Reservation res : reservationsDay) {
				if(res.getIdResa() != reservationIdPending) {
					nmbreCouvertsJour += res.getNbCouvertsResa();
		
					if(res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau1Midi().getIdCreneau()) {
						nmbreCouvertsCreneau1Midi += res.getNbCouvertsResa();
					}else if(parametresApp.getCreneau2Midi() != null && res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau2Midi().getIdCreneau()) {
						nmbreCouvertsCreneau2Midi += res.getNbCouvertsResa();
					}else if(parametresApp.getCreneau3Midi() != null && res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau3Midi().getIdCreneau()) {
						nmbreCouvertsCreneau3Midi += res.getNbCouvertsResa();
					}else if(res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau1Soir().getIdCreneau()) {
						nmbreCouvertsCreneau1Soir += res.getNbCouvertsResa();
					}else if(parametresApp.getCreneau2Soir() != null && res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau2Soir().getIdCreneau()) {
						nmbreCouvertsCreneau2Soir += res.getNbCouvertsResa();
					}else if(parametresApp.getCreneau3Soir() != null && res.getCreneauResa().getIdCreneau() == parametresApp.getCreneau3Soir().getIdCreneau()) {
						nmbreCouvertsCreneau3Soir += res.getNbCouvertsResa();
					}
				
				}
			}
		}

		

		//check if there's still enough booking available for each service
		if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau1Midi().getIdCreneau() && (nmbreCouvertsCreneau1Midi + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau1MidiNbResas())) {
			response = true;
		}
		if(parametresApp.getCreneau2Midi() != null) {
			if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau2Midi().getIdCreneau() && (nmbreCouvertsCreneau2Midi + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau2MidiNbResas())) {
				response = true;
			}
		}
		if(parametresApp.getCreneau3Midi() != null) {
			if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau3Midi().getIdCreneau() && (nmbreCouvertsCreneau3Midi + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau3MidiNbResas())) {
				response = true;
			}
		}
		if(parametresApp.getCreneau1Soir() != null) {
			if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau1Soir().getIdCreneau() && (nmbreCouvertsCreneau1Soir + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau1SoirNbResas())) {
				response = true;
			}
		}
		if(parametresApp.getCreneau2Soir() != null) {
			if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau2Soir().getIdCreneau() && (nmbreCouvertsCreneau2Soir + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau2SoirNbResas())) {
				response = true;
			}
		}
		if(parametresApp.getCreneau3Soir() != null) {
			if(validateReservation.getCreneauResa().getIdCreneau() == parametresApp.getCreneau3Soir().getIdCreneau() && (nmbreCouvertsCreneau3Soir + validateReservation.getNbCouvertsResa() <= parametresApp.getCreneau3SoirNbResas())) {
				response = true;
			}
		}
		
		return response;
	}
	
}
