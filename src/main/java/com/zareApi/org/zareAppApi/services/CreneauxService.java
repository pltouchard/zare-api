package com.zareApi.org.zareAppApi.services;

import java.sql.Date;
import java.util.List;

import com.zareApi.org.zareAppApi.entities.Creneau;

public interface CreneauxService {

	public List<Creneau> getListeCreneauxByDateAndDispo(Date date);
}
