package com.zareApi.org.zareAppApi.services;

import java.io.IOException;

import com.zareApi.org.zareAppApi.entities.ParametresParticuliersDto;

public interface ParametresParticuliersService {

	public boolean ajouterPlageDeDates(ParametresParticuliersDto parametresParticuliersDto) throws IOException, InterruptedException;
	public boolean ajouterMultiDates(ParametresParticuliersDto parametresParticuliersDto) throws IOException, InterruptedException;
}
