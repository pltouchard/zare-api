package com.zareApi.org.zareAppApi.services.impl;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zareApi.org.zareAppApi.entities.ParametresParticuliers;
import com.zareApi.org.zareAppApi.entities.ParametresParticuliersDto;
import com.zareApi.org.zareAppApi.repositories.ParametresParticuliersRepository;
import com.zareApi.org.zareAppApi.services.ParametresParticuliersService;

@Service
public class ParametresParticuliersServiceImpl implements ParametresParticuliersService {
	
	@Autowired
	ParametresParticuliersRepository parametresParticuliersRepository;

	@Override
	public boolean ajouterPlageDeDates(ParametresParticuliersDto parametresParticuliersDto)
			throws IOException, InterruptedException {

		ArrayList<LocalDate> testDates = new ArrayList<LocalDate>();
		for(String date : parametresParticuliersDto.getDates()) {
			testDates.add(LocalDate.parse(date));
		}
		for(LocalDate date = testDates.get(0); date.isBefore(testDates.get(1).plusDays(1)); date = date.plusDays(1)) {
			ParametresParticuliers parametreParticuliersAAjouter = parametresParticuliersDto.getParametreParticulier();
			parametreParticuliersAAjouter.setIdParamG(Date.valueOf(date));
			ParametresParticuliers paramsAjoutOk = parametresParticuliersRepository.saveAndFlush(parametreParticuliersAAjouter);
			if(paramsAjoutOk == null) {
				return false;
			}
			
		}
		
		return true;
	}
	
	@Override
	public boolean ajouterMultiDates(ParametresParticuliersDto parametresParticuliersDto)
			throws IOException, InterruptedException {

		ArrayList<LocalDate> testDates = new ArrayList<LocalDate>();
		for(String date : parametresParticuliersDto.getDates()) {
			testDates.add(LocalDate.parse(date));
		}
		for(LocalDate date : testDates) {
			ParametresParticuliers parametreParticuliersAAjouter = parametresParticuliersDto.getParametreParticulier();
			parametreParticuliersAAjouter.setIdParamG(Date.valueOf(date));
			ParametresParticuliers paramsAjoutOk = parametresParticuliersRepository.saveAndFlush(parametreParticuliersAAjouter);
			if(paramsAjoutOk == null) {
				return false;
			}
			
		}
		
		return true;
	}

}
