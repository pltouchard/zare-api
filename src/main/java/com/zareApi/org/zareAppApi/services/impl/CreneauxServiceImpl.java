package com.zareApi.org.zareAppApi.services.impl;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zareApi.org.zareAppApi.entities.Creneau;
import com.zareApi.org.zareAppApi.entities.Parametres;
import com.zareApi.org.zareAppApi.entities.ParametresParticuliers;
import com.zareApi.org.zareAppApi.repositories.ParametresParticuliersRepository;
import com.zareApi.org.zareAppApi.repositories.ParametresRepository;
import com.zareApi.org.zareAppApi.services.CreneauxService;

@Service
public class CreneauxServiceImpl implements CreneauxService {
	
	@Autowired
	ParametresRepository parametresRepository;
	
	@Autowired
	ParametresParticuliersRepository parametresParticuliersRepository;

	@Override
	public List<Creneau> getListeCreneauxByDateAndDispo(Date date) {
		List<Creneau> tousLesCreneaux = new ArrayList<Creneau>();
		
	    //get parametres particuliers for the date if exists
	    Optional<ParametresParticuliers> getParametreParticulier = parametresParticuliersRepository.findById(date);
	    
	    //if parametres particuliers exist for the date, get all creneaux by service available
		if(getParametreParticulier.isPresent()) {
			ParametresParticuliers parametreParticulier = getParametreParticulier.get();
			if(parametreParticulier.isOuvertMidiP()) {
				if(parametreParticulier.getCreneau1Midi() != null) {
					tousLesCreneaux.add(parametreParticulier.getCreneau1Midi());
				}
				if(parametreParticulier.getCreneau2Midi() != null) {
					tousLesCreneaux.add(parametreParticulier.getCreneau2Midi());
				}
				if(parametreParticulier.getCreneau3Midi() != null) {
					tousLesCreneaux.add(parametreParticulier.getCreneau3Midi());
				}
			}
			if(parametreParticulier.isOuvertSoirP()) {
				if(parametreParticulier.getCreneau1Soir() != null) {
					tousLesCreneaux.add(parametreParticulier.getCreneau1Soir());
				}
				if(parametreParticulier.getCreneau2Soir() != null) {
					tousLesCreneaux.add(parametreParticulier.getCreneau2Soir());
				}
				if(parametreParticulier.getCreneau3Soir() != null) {
					tousLesCreneaux.add(parametreParticulier.getCreneau3Soir());
				}
			}
			
			Collections.sort(tousLesCreneaux, new Comparator<Creneau>() {
				@Override
				public int compare(Creneau c1, Creneau c2) {
					return c1.getCreneauDebut().compareTo(c2.getCreneauDebut());
				}
			});
			
			return tousLesCreneaux;
		}
			
		//if there's no parametres particuliers for the date, get parametres generaux by service available
		
		//get parametres generaux
		Parametres parametres = parametresRepository.getOne(1);
		
		// arrays to match week days with available services 
		boolean[] ouverturesMidi = new boolean[]{
				parametres.isOuvertDimancheMidiG(),
				parametres.isOuvertLundiMidiG(),
				parametres.isOuvertMardiMidiG(),
				parametres.isOuvertMercrediMidiG(),
				parametres.isOuvertJeudiMidiG(),
				parametres.isOuvertVendrediMidiG(),
				parametres.isOuvertSamediMidiG()
				};
		boolean[] ouverturesSoir = new boolean[] {
				parametres.isOuvertDimancheSoirG(),
				parametres.isOuvertLundiSoirG(),
				parametres.isOuvertMardiSoirG(),
				parametres.isOuvertMercrediSoirG(),
				parametres.isOuvertJeudiSoirG(),
				parametres.isOuvertVendrediSoirG(),
				parametres.isOuvertSamediSoirG()
		};
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
	    int selectedDay = cal.get(Calendar.DAY_OF_WEEK);
	    
	    //get creneaux by available service of the restaurant
	    if(ouverturesMidi[selectedDay-1]) {
			tousLesCreneaux.add(parametres.getCreneau1Midi());
			if(parametres.getCreneau2Midi() != null) {
				tousLesCreneaux.add(parametres.getCreneau2Midi());
			}
			if(parametres.getCreneau3Midi() != null) {
				tousLesCreneaux.add(parametres.getCreneau3Midi());
			}
	    }

	    if(ouverturesSoir[selectedDay-1]) {
			tousLesCreneaux.add(parametres.getCreneau1Soir());
			if(parametres.getCreneau2Soir() != null) {
				tousLesCreneaux.add(parametres.getCreneau2Soir());
			}
			if(parametres.getCreneau3Soir() != null) {
				tousLesCreneaux.add(parametres.getCreneau3Soir());
			}
	    }
	    
		Collections.sort(tousLesCreneaux, new Comparator<Creneau>() {
			@Override
			public int compare(Creneau c1, Creneau c2) {
				return c1.getCreneauDebut().compareTo(c2.getCreneauDebut());
			}
		});

		return tousLesCreneaux;

	}

}
