package com.zareApi.org.zareAppApi.entities;

import java.io.Serializable;
import java.util.ArrayList;

public class ParametresParticuliersDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ParametresParticuliers parametresParticuliers;
	private ArrayList<String> dates;
	
	public ParametresParticuliersDto() {
		super();
	}

	public ParametresParticuliersDto(ParametresParticuliers parametresParticuliers, ArrayList<String> dates) {
		super();
		this.parametresParticuliers = parametresParticuliers;
		this.dates = dates;
	}

	public ParametresParticuliers getParametreParticulier() {
		return parametresParticuliers;
	}

	public void setParametreParticulier(ParametresParticuliers parametresParticuliers) {
		this.parametresParticuliers = parametresParticuliers;
	}

	public ArrayList<String> getDates() {
		return dates;
	}

	public void setDates(ArrayList<String> dates) {
		this.dates = dates;
	}
	
	
	
}
