package com.zareApi.org.zareAppApi.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Entity
@Table(name = "reservations")
public class Reservation implements Serializable {

	private static final long serialVersionUID = 886930109217216768L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idResa;
	
	@Column(name = "date_resa")
	private Date dateResa;
	
	@Column(name = "nom_resa")
	@Size(min = 3, max = 100)
	private String nomResa;
	
	@Column(name =  "telephone_resa")
	@Size(min = 10, max = 15)
	private String telephoneResa;
	
	@Column(name = "mail_resa")
	@Size(min = 6, max = 80)
	private String mailResa;
	
	@Column(name = "nb_couverts_resa")
	@Min(1)
	@Max(15)
	private int nbCouvertsResa;
	
	@ManyToOne
	@JoinColumn(name="creneau_resa")
	private Creneau creneauResa;
	
	@Column(name = "service_resa")
	private String serviceResa;
	
	@Column(name = "show_resa")
	private boolean showResa;
	
	@Column(name = "notes_resa")
	private String notesResa;
	
	@Column(name = "is_notes_resa_important")
	private boolean notesResaImportant;
	
	@Column(name = "is_habitue")
	private boolean habitue;

	public Reservation() {
		super();
	}


//	public Reservation(Date dateResa, @Size(min = 3, max = 100) String nomResa, String telephoneResa,
//			@Size(min = 6, max = 80) String mailResa, @Min(1) @Max(15) int nbCouvertsResa, int creneauResa,
//			String serviceResa, boolean showResa) {
//		super();
//		this.dateResa = dateResa;
//		this.nomResa = nomResa;
//		this.telephoneResa = telephoneResa;
//		this.mailResa = mailResa;
//		this.nbCouvertsResa = nbCouvertsResa;
//		this.creneauResa = creneauRepository.getOne(creneauResa);
//		this.serviceResa = serviceResa;
//		this.showResa = showResa;
//	}



	public Reservation(int idResa, Date dateResa, @Size(min = 3, max = 100) String nomResa,
		@Size(min = 10, max = 15) String telephoneResa, @Size(min = 6, max = 80) String mailResa,
		@Min(1) @Max(15) int nbCouvertsResa, Creneau creneauResa, String serviceResa, boolean showResa,
		String notesResa, boolean notesResaImportant, boolean habitue) {
		super();
		this.idResa = idResa;
		this.dateResa = dateResa;
		this.nomResa = nomResa;
		this.telephoneResa = telephoneResa;
		this.mailResa = mailResa;
		this.nbCouvertsResa = nbCouvertsResa;
		this.creneauResa = creneauResa;
		this.serviceResa = serviceResa;
		this.showResa = showResa;
		this.notesResa = notesResa;
		this.notesResaImportant = notesResaImportant;
		this.habitue = habitue;
	}



	public Reservation(Date dateResa, @Size(min = 3, max = 100) String nomResa,
		@Size(min = 10, max = 15) String telephoneResa, @Size(min = 6, max = 80) String mailResa,
		@Min(1) @Max(15) int nbCouvertsResa, Creneau creneauResa, String serviceResa, boolean showResa,
		String notesResa, boolean notesResaImportant, boolean habitue) {
	super();
	this.dateResa = dateResa;
	this.nomResa = nomResa;
	this.telephoneResa = telephoneResa;
	this.mailResa = mailResa;
	this.nbCouvertsResa = nbCouvertsResa;
	this.creneauResa = creneauResa;
	this.serviceResa = serviceResa;
	this.showResa = showResa;
	this.notesResa = notesResa;
	this.notesResaImportant = notesResaImportant;
	this.habitue = habitue;
}


	public int getIdResa() {
		return idResa;
	}



	public Date getDateResa() {
		return dateResa;
	}



	public void setDateResa(Date dateResa) {
		this.dateResa = dateResa;
	}



	public String getNomResa() {
		return nomResa;
	}



	public void setNomResa(String nomResa) {
		this.nomResa = nomResa;
	}



	public String getTelephoneResa() {
		return telephoneResa;
	}



	public void setTelephoneResa(String telephoneResa) {
		this.telephoneResa = telephoneResa;
	}



	public String getMailResa() {
		return mailResa;
	}



	public void setMailResa(String mailResa) {
		this.mailResa = mailResa;
	}



	public int getNbCouvertsResa() {
		return nbCouvertsResa;
	}



	public void setNbCouvertsResa(int nbCouvertsResa) {
		this.nbCouvertsResa = nbCouvertsResa;
	}



	public Creneau getCreneauResa() {
		return creneauResa;
	}



	public void setCreneauResa(Creneau creneauResa) {
		this.creneauResa = creneauResa;
	}
	




	public String getServiceResa() {
		return serviceResa;
	}



	public void setServiceResa(String serviceResa) {
		this.serviceResa = serviceResa;
	}



	public boolean isShowResa() {
		return showResa;
	}



	public void setShowResa(boolean showResa) {
		this.showResa = showResa;
	}




	public String getNotesResa() {
		return notesResa;
	}


	public void setNotesResa(String notesResa) {
		this.notesResa = notesResa;
	}

	


	public boolean isNotesResaImportant() {
		return notesResaImportant;
	}


	public void setNotesResaImportant(boolean notesResaImportant) {
		this.notesResaImportant = notesResaImportant;
	}


	public boolean isHabitue() {
		return habitue;
	}


	public void setHabitue(boolean habitue) {
		this.habitue = habitue;
	}


	@Override
	public String toString() {
		return "Reservation [idResa=" + idResa + ", dateResa=" + dateResa + ", nomResa=" + nomResa + ", telephoneResa="
				+ telephoneResa + ", mailResa=" + mailResa + ", nbCouvertsResa=" + nbCouvertsResa + ", creneauResa="
				+ creneauResa + ", serviceResa=" + serviceResa + ", showResa=" + showResa + ", notesResa=" + notesResa
				+ ", notesResaImportant=" + notesResaImportant +  ", habitue=" + habitue + "]";
	}


	
	
	
	
	
	
	
}
