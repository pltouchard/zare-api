package com.zareApi.org.zareAppApi.repositories;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.zareApi.org.zareAppApi.entities.ParametresParticuliers;

public interface ParametresParticuliersRepository extends JpaRepository<ParametresParticuliers, Date>{
	@Query(value="SELECT * from parametres_particuliers p where p.id_param_p=?1", nativeQuery = true)
	List<ParametresParticuliers> findParametresParticuliersByDate(Date dateParamPDate1);
	
	@Query(value="SELECT * from parametres_particuliers p where p.id_param_p>=?1 AND p.id_param_p <= ?2", nativeQuery = true)
	List<ParametresParticuliers> findAllParametresParticuliersByDate(Date dateParamPDate1, Date dateParamPDate2);

}
