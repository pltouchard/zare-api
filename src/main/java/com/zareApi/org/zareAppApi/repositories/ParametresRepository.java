package com.zareApi.org.zareAppApi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zareApi.org.zareAppApi.entities.Parametres;

public interface ParametresRepository  extends JpaRepository<Parametres, Integer> {

}
