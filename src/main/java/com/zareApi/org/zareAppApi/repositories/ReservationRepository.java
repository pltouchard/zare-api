package com.zareApi.org.zareAppApi.repositories;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.zareApi.org.zareAppApi.entities.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {
	List<Reservation> findByNomResaContaining(String nomResa);
	
	@Query(value="select * from reservations r where r.date_resa = ?1", nativeQuery = true)
	List<Reservation> findByDateResaContaining(Date dateResa);

	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value="update reservations r set r.date_resa = ?1, r.nom_resa = ?2, r.telephone_resa = ?3, r.mail_resa = ?4, r.nb_couverts_resa = ?5, r.creneau_resa = ?6, r.service_resa = ?7, r.show_resa = ?8, r.notes_resa = ?9, r.is_notes_resa_important = ?10, r.is_habitue = ?11 where r.id_resa = ?12", nativeQuery = true)
	void update(Date dateResa, String nomResa, String telephoneResa, String mailResa, int nbCouvertsResa, int creneauResa, String serviceResa, boolean showResa, String notesResa, boolean notesResaImportant, boolean habitue, int idResa );
	
	@Query(value="select * from reservations r where r.date_resa >= ?1 AND r.date_resa <= ?2", nativeQuery = true)
	List<Reservation> findReservationByPlageDates(Date dateDebut, Date dateFin);
}
