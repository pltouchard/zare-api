package com.zareApi.org.zareAppApi.repositories;

import java.sql.Time;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.zareApi.org.zareAppApi.entities.Creneau;

public interface CreneauRepository extends JpaRepository<Creneau, Integer> {

	@Query(value="select * from creneau c where c.creneau_debut = ?1 and c.creneau_fin = ?2", nativeQuery = true)
	Creneau creneauClaimed(Time creneauDebut, Time creneaufin);
	
    Creneau findById(int id);

}
