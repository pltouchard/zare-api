package com.zareApi.org.zareAppApi.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.zareApi.org.zareAppApi.entities.UserInfo;

public interface UserInfoRepository extends JpaRepository<UserInfo,Integer>{

    Boolean existsByUsername(String username);
    UserInfo findByUsername(String username);
}
