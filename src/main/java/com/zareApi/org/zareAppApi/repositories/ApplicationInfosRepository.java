package com.zareApi.org.zareAppApi.repositories;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.zareApi.org.zareAppApi.entities.ApplicationInfos;

public interface ApplicationInfosRepository extends JpaRepository<ApplicationInfos, Integer> {
	
	ApplicationInfos findById(int id);
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value="update application_infos a set a.param_generaux_set = ?1, a.date_set_param_generaux = ?2 where a.id = ?3", nativeQuery = true)
	void update(boolean paramGenerauxSet, Date setParamGeneraux, int id );
}
